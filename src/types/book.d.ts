
export interface Book {
  id: number
  author: string
  title: string
  imageLink: string
  links: object
  description: string,
  rating: number
  year: number
}
