import Vue from 'vue'
import Router from 'vue-router'
import Books from './views/books.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '*',
      redirect: '/books'
    },
    {
      path: '/books',
      name: 'books',
      component: Books
    }
  ]
})
