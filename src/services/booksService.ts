import axios, { AxiosResponse } from 'axios'
import { Book } from '@/types/book'
const host = 'http://localhost:3000/'
const pageLimit = 5

const findPageInUrl = (url: string = ''): number | null => {
  const [, query] = url.split('?')
  const pageSearch = new URLSearchParams(query)
  const page = pageSearch.get('_page')
  return page ? parseInt(page) : null
}

const switchcase = (cases: any) => (key: string) =>
  cases.hasOwnProperty(key) ? cases[key] : null

const relSwitchCase = switchcase({
  'rel="first"': 'first',
  'rel="prev"': 'prev',
  'rel="next"': 'next',
  'rel="last"': 'last'
})

export async function fetchBooks (pageLink?: string) {
  try {
    const url = pageLink || `${host}books?_page=1&_limit=${pageLimit}`
    const response: AxiosResponse = await axios.get(url)

    const page = findPageInUrl(url)

    const pagination = response.headers.link.split(',')
    const links: any = {}

    for (let p of pagination) {
      const [link, rel] = p.split(';')
      const key = relSwitchCase(rel.trim())
      if (key) links[key] = link.replace(/[<>]/g, '').trim()
    }

    const lastPage = findPageInUrl(links.last)

    return { books: response.data, pagination: { page, lastPage, ...links } }
  } catch (error) {
    console.error(error)
    return {}
  }
}
