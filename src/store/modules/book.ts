import * as shopService from '@/services/booksService'
import { Book } from '@/types/book'

export const namespaced = true

export const state = {
  books: [] as Book[],
  pagination: {} as object
}

export const mutations = {
  SET_BOOKS (state: any, books: Book[]) {
    if (books) state.books = books
  },
  SET_PAGINATION (state: any, pagination: object) {
    if (pagination) state.pagination = pagination
  }
}

export const actions = {
  async fetchBooks ({ commit, getters }: {commit: any, getters: any}, pageLink?: string) {
    const { books, pagination } = await shopService.fetchBooks(pageLink)
    commit('SET_BOOKS', books)
    commit('SET_PAGINATION', pagination)
  }
}

export const getters = {
  getPagination: (state: any) => state.pagination
}
