import Vue from 'vue'
import Vuex from 'vuex'

// modules
import * as book from '@/store/modules/book'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    book
  },
  state: {}
})

export default store
