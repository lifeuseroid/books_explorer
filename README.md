## VueJS Frontend Challenge

### Challenge

![VueJS 1](./images/node_4-01.png)

![VueJS 2](./images/node_4-02.png)

- Each page should display 5 books.
- A few pages should be available in order for pagination to work.
- Book entries should be clickable and expand/collapse to show/hide more information about the selected book.
- Book stores should only be displayed when the respective URL is available; make different entries as represented on the images so different store availability scenarios are represented.
- Improve the UI as you think works best.

Add test coverage as you see fit.

The project should be responsible for managing all the required dependencies and should run just by using `npm install` and `npm start`.

### Instructions

`cd books_explorer/`  
`npm install`  
`npm run rest (it will spin https://github.com/typicode/json-server with /books_explorer/db.json)`  
`npm run start (it will build with vue-cli-service which is based on Webpack and serve /dist with https://github.com/zeit/serve)`  

`http://localhost:5000`  
