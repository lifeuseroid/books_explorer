module.exports = {
  root: true,
  env: {
    node: true
  },

  extends: [
    'plugin:vue/essential',
    '@vue/standard',
    '@vue/typescript',
    'prettier/standard'
  ],
  plugins: [
    'import',
    'prettier',
    'standard',
    'typescript'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'vue/script-indent': [
      'error',
      2,
      {
        'baseIndent': 1
      }
    ],
    'vue/html-indent': [
      'error',
      2,
      {
        'attribute': 1,
        'baseIndent': 1,
        'closeBracket': 0,
        'alignAttributesVertically': true,
        'ignores': []
      }
    ]
  },
  'overrides': [
    {
      'files': [
        '*.vue'
      ],
      'rules': {
        'indent': 'off'
      }
    }
  ],
  'parserOptions': {
    'parser': 'typescript-eslint-parser',
    'ecmaFeatures': {
      'jsx': false
    }
  }
}
